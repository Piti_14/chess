
public class Game {
	
	Board b;
	Movement[] moves;
	int numMoves = 0;
	
	public Game() {
		b = new Board();
		moves = new Movement[10000];	
	}
	
	public void movePiece(Movement m) {
		if(b.movePiece(m)) {
			moves[numMoves] = m;
			numMoves++;
			
		} else {
			System.out.println("Invalid movement");
		}
	}
	
	
	@Override
	public String toString() {
		String s = "List of Moves: ";
		for(int i = 0; i < moves.length; i++) {
			if(moves[i] != null) {
				s += "\n" + (i +1) + ".- [ " + moves[i] + " ]";
			}
		}
		return s + "\n" + b.toString();
	}

	protected int getNumMoves() {
		return numMoves;
	}

	protected void setNumMoves(int numMoves) {
		this.numMoves = numMoves;
	}

	protected Board getB() {
		return b;
	}

	protected Movement[] getMoves() {
		return moves;
	}
	
	

}
