
public class Movement {
	public Piece p;
	public char initialCol, destinationCol;
	public int initialRow, destinationRow;
	
	public Movement(Piece piece, char originCol, int originRow, char finalCol, int finalRow) {
		p = piece;
		initialCol = originCol;
		initialRow = originRow;
		destinationCol = finalCol;
		destinationRow = finalRow;
	}
	
	@Override
	public String toString() {
		return p + " from " + initialCol + initialRow + " to " + destinationCol + destinationRow;
	}
}
