
public class Board {
	public static final int DIMENSION = 8;
	private Piece[][] board;
	
	public Board() {
		board = new Piece[DIMENSION][DIMENSION];
		
		board[0][0] = new Piece(TypeOfPiece.TOWER, Color.WHITE);
		board[0][1] = new Piece(TypeOfPiece.KNIGHT, Color.WHITE);
		board[0][2] = new Piece(TypeOfPiece.BISHOP, Color.WHITE);
		board[0][3] = new Piece(TypeOfPiece.QUEEN, Color.WHITE);
		board[0][4] = new Piece(TypeOfPiece.KING, Color.WHITE);
		board[0][5] = new Piece(TypeOfPiece.BISHOP, Color.WHITE);
		board[0][6] = new Piece(TypeOfPiece.KNIGHT, Color.WHITE);
		board[0][7] = new Piece(TypeOfPiece.TOWER, Color.WHITE);
		
		board[7][0] = new Piece(TypeOfPiece.TOWER, Color.BLACK);
		board[7][1] = new Piece(TypeOfPiece.KNIGHT, Color.BLACK);
		board[7][2] = new Piece(TypeOfPiece.BISHOP, Color.BLACK);
		board[7][3] = new Piece(TypeOfPiece.QUEEN, Color.BLACK);
		board[7][4] = new Piece(TypeOfPiece.KING, Color.BLACK);
		board[7][5] = new Piece(TypeOfPiece.BISHOP, Color.BLACK);
		board[7][6] = new Piece(TypeOfPiece.KNIGHT, Color.BLACK);
		board[7][7] = new Piece(TypeOfPiece.TOWER, Color.BLACK);
		
		for(int i = 0; i < board.length; i++) {
			board[1][i] = new Piece(TypeOfPiece.PAWN, Color.WHITE);
			board[2][i] = null;
			board[3][i] = null;
			board[4][i] = null;
			board[5][i] = null;
			board[6][i] = new Piece(TypeOfPiece.PAWN, Color.BLACK);
		}
	}
	
	public String toString() {
		String s = "";		
		for(int i = board.length - 1; i >= 0; i--) {
			s += (i + 1) + "  ";
			for(int j = 0; j < board[0].length; j++) {
				if(board[i][j] != null) {
				s += board[i][j].onBoardString() + " ";
				} else {
					s += "· ";
				}
			}
			s += "\n";
		}
		s += "   A B C D E F G I";
		return s;
	}
	
	public Piece getPiece(char col, int row) {
		int boardRow = row - 1;
		int boardCol = col - 'A';
		return board[boardRow][boardCol];
	}
	
	public boolean movePiece(Movement move) {
		Piece piece = move.p;
		int rowOrigin = move.initialRow - 1;
		int colOrigin = move.initialCol - 'A';
		int rowDest = move.destinationRow - 1;
		int colDest = move.destinationCol - 'A';
		
		if(rowOrigin < 0 || rowOrigin > DIMENSION 
			|| rowDest < 0 || rowDest > DIMENSION
			||	colOrigin < 0 || colOrigin > DIMENSION
			|| colDest < 0 || colDest > DIMENSION) {
			
			return false;
			
		} else if(piece != board[rowOrigin][colOrigin]) {
			return false;
		} else {
			board[rowOrigin][colOrigin] = null;
			board[rowDest][colDest] = piece;
			return true;
		}
	}
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

