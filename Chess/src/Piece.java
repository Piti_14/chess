
public class Piece {
	private TypeOfPiece type;
	private Color color;
	
	public Piece(TypeOfPiece type, Color color) {
		this.type = type;
		this.color = color;
	}
	
	@Override
	public String toString() {
		return "" + color + " " + type;
	}

	protected TypeOfPiece getType() {
		return type;
	}

	protected void setType(TypeOfPiece type) {
		this.type = type;
	}

	protected Color getColor() {
		return color;
	}
	
	public String onBoardString() {
		String t = "";
		switch(type) {
			case PAWN:
				t = "P";
				break;
			case TOWER:
				t = "T";
				break;
			case KNIGHT:
				t = "K";
				break;
			case QUEEN:
				t = "Q";
				break;
			case KING:
				t = "X";
				break;
			case BISHOP:
				t = "B";
				break;
		}
		
		if(color == Color.WHITE) {
			t = t.toLowerCase(); 
		}
		return t;
	}
}
